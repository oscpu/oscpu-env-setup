# oscpu-env-setup

#### 介绍
`一生一芯`开发环境一键搭建脚本。

#### 使用说明

`一生一芯`开发环境要求使用`Ubuntu v20.04`，本脚本只支持在`Ubuntu v20.04`下使用。可使用以下命令搭建开发环境。

```shell
# 选择基于verilog语言开发
wget https://gitee.com/oscpu/oscpu-env-setup/raw/master/oscpu-env-setup.sh && chmod +x oscpu-env-setup.sh && ./oscpu-env-setup.sh -g && rm oscpu-env-setup.sh
# 选择基于chisel语言开发
wget https://gitee.com/oscpu/oscpu-env-setup/raw/master/oscpu-env-setup.sh && chmod +x oscpu-env-setup.sh && ./oscpu-env-setup.sh -g -c && rm oscpu-env-setup.sh
```

